# ihk-abschlussarbeit

Final papers for my apprenticeship @SUSE. Due to the fact that this paper is in german, the content of this repository will be fully german!

## Installation

```
sudo zypper in -y texlive texlive-eqlist texlive-lastpage python3-pygments texlive-glossaries-german
```

## Wie wurde diese Arbeit erstellt?

Ich habe VSCode für das Erstellen des Dokumentes mit dem Plugin LaTeX Workshop benutzt.

Zusätzlich mache ich mir für die Code Listings <https://github.com/gpoore/minted> zunutze.

Zum Erstellen der Tabellen behelfe ich mir mit <https://www.tablesgenerator.com/latex_tables#>

Gitignore geklaut von: <https://github.com/github/gitignore/blob/master/TeX.gitignore>

## Themen in der Projektarbeit

Dieser Abschnitt wird eine manuell gepflegte Liste mit allen Themen auf die ich mich für das Fachgespräch vorbereiten
muss beinhalten.

Themen:

- API
- Betriebssystem
- CD
- CI
- Cobbler
- Commit
- Decorator (Python)
- Git
- Github
- Github Actions
- Gitter
- JSON
- Kanban
- Kundenauftrag
- Lasten- & Pflichtenheft
- Objekt
- Open Source
- Parser
- Projekt
- PyPi
- Pytest
- Python
- Python Tuple
- Scrum
- Testarten
- Tests
- Version
- `yield` in Python

## Überlegungen für die Projektpräsentation

- Wo bekomme ich einen eigenen Beamer her mit entsprechender Lichtstärke?
- Was bereite ich vor und was mache ich interaktiv? - pytest Testlauf & CLI-Demo
- reveal.js als Präsentation geeignet für die IHK?
